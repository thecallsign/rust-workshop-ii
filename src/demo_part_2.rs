use rayon::prelude::*;
use std::cell::Cell;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::Arc;
use std::time::Instant;
use tokio::prelude::*;
use tokio::sync::Mutex;

trait Walk {
    fn walk(&self);
}

trait Run {
    fn run(&self);
}

#[derive(Debug)]
struct Cat;

struct Dog;

impl Walk for Cat {
    fn walk(&self) {
        println!("Cat walking")
    }
}

impl Walk for Dog {
    fn walk(&self) {
        println!("Dog walking")
    }
}

impl<T> Run for T
where
    T: Walk,
{
    fn run(&self) {
        do_run()
    }
}

fn do_run() {
    // lots of code
}

#[tokio::main]
async fn main() {
    let mut data = String::from("ABC");

    let data = Arc::new(Mutex::new(data));

    tokio::spawn({
        let data = Arc::clone(&data);
        async move {
            *data.lock().await += "DEF";
        }
    });

    let mut sock = tokio::net::TcpListener::bind("0.0.0.0:0").await.unwrap();

    while let Ok((mut stream, _)) = sock.accept().await {
        // pub async fn accept(&mut self) -> io::Result<(TcpStream, SocketAddr)
        tokio::spawn(async move {
            stream.write(b"123").await.unwrap();
        });
    }
}

// using Rayon
// let numbers: Vec<f64> = (0..1_000_000).map(|i| i as f64).collect();
//
// let base_iter = numbers.par_iter().map(|item| item * item);
//
// let start = Instant::now();
//
// let cubed: Vec<_> = base_iter.clone().map(|item| item * item).collect();
// let cubed: Vec<_> = base_iter.clone().map(|item| item * item).collect();
// let squared: Vec<_> = base_iter.clone().collect();
//
// println!("Took: {:?}", Instant::now().duration_since(start));

// let dog = Dog;
// let cat = Cat;
//
// cat.walk();
// dog.run();
//
// let num = 5;
// let boxed_num = Box::new(5);

// let zoo: Vec<Box<dyn Walk>> = vec![Box::new(dog), Box::new(cat)];
//
// zoo.iter().for_each(|animal| {
//     animal.walk();
// });

// println!("{:?}", cat);
// println!("{:?}", dog);

// let mut data = String::from("ABC");
//
// let data = Arc::new(Mutex::new(data));
// let handle = std::thread::spawn({
//     let data = Arc::clone(&data); // still parent thread
//     move || {
//         // child thread
//         *data.lock().unwrap() += "DEF";
//     }
// });
//
// println!("Data: {:?}", data);
// handle.join().unwrap();
// println!("Data: {:?}", data);
// here
