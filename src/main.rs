use anyhow::bail;
use anyhow::Result;

#[derive(Debug)]
struct Rental {
    state: RentalState,
}

#[derive(Debug)]
enum RentalState {
    Open {
        available: Vec<Car>,
        leased: Vec<Car>,
    },
    Closed {
        manager: AfterHoursManager,
    },
}

#[derive(Debug)]
struct AfterHoursManager {
    cars_available: Vec<Car>,
    cars_leased: Vec<Car>,
    name: &'static str,
}

#[derive(Debug, Default)]
struct Car {
    kind: CarKind,
    color: Color,
}
#[derive(Debug)]
enum CarKind {
    Sudan,
    Hatchback,
    SUV,
}

#[derive(Debug, Default)]
struct Color(u8, u8, u8);

impl Rental {
    pub fn new(cars: Vec<Car>) -> Rental {
        Self {
            state: RentalState::Closed {
                manager: AfterHoursManager {
                    cars_available: cars,
                    cars_leased: vec![],
                    name: "",
                },
            },
        }
    }

    pub fn open(&mut self) -> Result<()> {
        match self.state {
            RentalState::Open { .. } => bail!("Can't open an already open rental shop!"),
            RentalState::Closed { manager } => {
                let (available, leased) = manager.into_parts();

                self.state = RentalState::Open {
                    available: manager.cars_available,
                    leased: vec![],
                };
                Ok(())
            }
        }
    }
}

impl AfterHoursManager {
    pub fn into_parts(self) -> (Vec<Car>, Vec<Car>) {
        (self.cars_available, self.cars_leased)
    }
}

impl Default for CarKind {
    fn default() -> Self {
        CarKind::Sudan
    }
}

fn main() {
    println!("Hello, world!");
}
