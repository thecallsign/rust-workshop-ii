use anyhow::bail;
use anyhow::Result;
use std::cell::{Cell, RefCell};
use std::mem::swap;
use std::ops::Deref;
use std::rc::Rc;

#[derive(Debug)]
struct Rental<'a> {
    state: RentalState<'a>,
    cars_leased: Cell<usize>, // unsigned 64 bit number on 64 systems
}

#[derive(Debug)]
enum RentalState<'a> {
    Open { cars: &'a CarStock },
    Closed { manager: AfterHoursManager<'a> },
    Opening,
}

#[derive(Debug)]
struct CarStock(Vec<Car>);

#[derive(Debug, Default)]
struct Car {
    kind: CarKind,
    color: Color,
}

#[derive(Debug)]
enum CarKind {
    Sudan,
    Hatchback,
    SUV,
}

#[derive(Debug, Default)]
struct Color(u8, u8, u8);

#[derive(Debug)]
struct AfterHoursManager<'a> {
    stock: &'a CarStock,
    name: &'static str,
}

impl<'a> AfterHoursManager<'a> {
    pub fn stock(&self) -> &'a CarStock {
        self.stock
    }
}

#[derive(Debug)]
struct RentedCar<'rental: 'car, 'car, 'stock> {
    car: &'car Car,                  // 8
    rental: &'rental Rental<'stock>, // 8
}

impl Deref for RentedCar<'_, '_, '_> {
    type Target = Car;

    fn deref(&self) -> &Self::Target {
        self.car
    }
}

impl Drop for RentedCar<'_, '_, '_> {
    fn drop(&mut self) {
        self.rental
            .cars_leased
            .set(self.rental.cars_leased.get() - 1)
    }
}

impl<'a> Rental<'a> {
    pub fn new(stock: &'a CarStock) -> Rental<'a> {
        Self {
            state: RentalState::Closed {
                manager: AfterHoursManager {
                    stock,
                    name: "Dave",
                },
            },
            cars_leased: Cell::new(0),
        }
    }

    pub fn open(&mut self) -> Result<()> {
        let mut temp_state = RentalState::Opening;

        match &mut self.state {
            RentalState::Open { .. } => bail!("Cannot open rental while it is already open!"),
            RentalState::Opening => bail!("Already opening the shop!"),
            state @ RentalState::Closed { .. } => {
                swap(&mut temp_state, state);

                if let RentalState::Closed { manager } = temp_state {
                    let AfterHoursManager { stock, .. } = manager;

                    self.state = RentalState::Open { cars: stock };
                }

                Ok(())
            }
        }
    }

    pub fn rent(&self) -> Result<RentedCar> {
        match self.state {
            RentalState::Open { cars } => {
                if self.cars_leased.get() == cars.0.len() {
                    bail!("No more cars left to rent!")
                }

                self.cars_leased.set(self.cars_leased.get() + 1);

                let rented_car = RentedCar {
                    car: &cars.0[self.cars_leased.get()],
                    rental: self,
                };

                Ok(rented_car)
            }
            RentalState::Closed { .. } => bail!("Rental is closed!"),
            RentalState::Opening => bail!("Shop is still opening!"),
        }
    }
}

impl Default for CarKind {
    fn default() -> Self {
        CarKind::Sudan
    }
}

fn main() {
    let fleet = CarStock(vec![Car::default(), Car::default(), Car::default()]);

    let mut agency = Rental::new(&fleet);

    println!("{:?}", agency);

    agency.open().unwrap();

    println!("{:?}", agency);

    {
        let car = agency.rent().unwrap();
        println!("{:?}", car);
        println!("{:?}", agency.cars_leased);
    } // car is dropped

    println!("{:?}", agency.cars_leased);

    let demo = "Dave";
    let demo_ref = Rc::new(RefCell::new(demo));
    let ref2 = Rc::clone(&demo_ref);
    demo_ref.borrow();
    ref2.borrow();

    {
        let a = demo_ref.borrow_mut();
        ref2.borrow_mut();
    }

    ref2.borrow();
}

fn demo(agency: &mut Rental) {
    let car = agency.rent().unwrap();
    println!("{:?}", car);
}
