use anyhow::bail;
use anyhow::Result;
use std::mem::swap;

#[derive(Debug)]
struct Rental {
    state: RentalState,
}

#[derive(Debug)]
enum RentalState {
    Open {
        available: Vec<Car>,
        leased: Vec<Car>,
    },
    Closed {
        manager: AfterHoursManager,
    },
    Opening,
}

#[derive(Debug)]
struct AfterHoursManager {
    cars_available: Vec<Car>,
    cars_leased: Vec<Car>,
    name: &'static str,
}

#[derive(Debug, Default)]
struct Car {
    kind: CarKind,
    color: Color,
}

#[derive(Debug)]
enum CarKind {
    Sudan,
    Hatchback,
    SUV,
}

#[derive(Debug, Default)]
struct Color(u8, u8, u8);

impl Rental {
    pub fn new(cars: Vec<Car>) -> Rental {
        Self {
            state: RentalState::Closed {
                manager: AfterHoursManager {
                    cars_available: cars,
                    cars_leased: vec![],
                    name: "Dave",
                },
            },
        }
    }

    pub fn open(&mut self) -> Result<()> {
        let mut temp_state = RentalState::Opening;

        match &mut self.state {
            RentalState::Open { .. } => bail!("Can't open an already open rental shop!"),
            RentalState::Opening { .. } => bail!("Already opening the shop!"),
            old_state @ RentalState::Closed { .. } => {
                swap(&mut temp_state, old_state);

                let manager = temp_state.unwrap_closed();
                let (available, leased) = manager.into_parts();

                self.state = RentalState::Open { available, leased };
                Ok(())
            }
        }
    }

    pub fn rent(&mut self) -> Result<&Car, anyhow::Error> {
        match self.state {
            RentalState::Open {
                ref mut available,
                ref mut leased,
            } => match available.pop() {
                None => bail!("No more cars left to rent!"),
                Some(car) => {
                    leased.push(car);
                    Ok(leased.last().unwrap())
                }
            },
            RentalState::Closed { .. } => bail!("Rental is closed!"),
            RentalState::Opening => bail!("Shop is still opening!"),
        }
    }
}

impl RentalState {
    pub fn unwrap_open(self) -> (Vec<Car>, Vec<Car>) {
        match self {
            RentalState::Open { available, leased } => (available, leased),
            RentalState::Closed { .. } => panic!("Tried to unwrap open on closed state"),
            RentalState::Opening => panic!("Tried to unwrap open on opening state"),
        }
    }

    pub fn unwrap_closed(self) -> AfterHoursManager {
        match self {
            RentalState::Closed { manager } => manager,
            RentalState::Open { .. } => panic!("Tried to unwrap closed on open state"),
            RentalState::Opening => panic!("Tried to unwrap closed on opening state"),
        }
    }
}

impl AfterHoursManager {
    pub fn into_parts(self) -> (Vec<Car>, Vec<Car>) {
        (self.cars_available, self.cars_leased)
    }
}

impl Default for CarKind {
    fn default() -> Self {
        CarKind::Sudan
    }
}

fn main() {
    let mut rental = Rental::new(vec![Car::default(), Car::default()]);

    let c = rental.rent();
    assert!(c.is_ok());
    let c = rental.rent();
    assert!(c.is_ok());
    let c = rental.rent();
    assert!(c.is_err());
}
