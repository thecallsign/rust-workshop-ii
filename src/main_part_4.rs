use anyhow::bail;
use anyhow::Result;
use std::mem::swap;

#[derive(Debug)]
struct Rental<'a> {
    state: RentalState<'a>,
    cars_leased: usize,
}

#[derive(Debug)]
struct CarStock(Vec<Car>);

#[derive(Debug)]
enum RentalState<'a> {
    Open { cars: &'a CarStock },
    Closed { manager: AfterHoursManager<'a> },
    Opening,
}

#[derive(Debug)]
struct AfterHoursManager<'a> {
    stock: &'a CarStock,
    name: &'static str,
}

#[derive(Default, Debug)]
struct Car {
    kind: CarKind,
    color: Color,
}

#[derive(Debug)]
enum CarKind {
    Sudan,
    Hatchback,
    SUV,
}

#[derive(Debug, Default)]
struct Color(u8, u8, u8);

#[derive(Debug)]
struct RentedCar<'rental: 'car, 'car, 'stock> {
    car: &'car Car,
    rental: &'rental Rental<'stock>,
}

impl<'a> Rental<'a> {
    pub fn new(stock: &'a CarStock) -> Rental<'a> {
        Self {
            state: RentalState::Closed {
                manager: AfterHoursManager {
                    stock,
                    name: "Dave",
                },
            },
            cars_leased: 0,
        }
    }

    pub fn open(&mut self) -> Result<()> {
        let mut temp_state = RentalState::Opening;

        match &mut self.state {
            RentalState::Open { .. } => bail!("Can't open an already open rental shop!"),
            RentalState::Opening { .. } => bail!("Already opening the shop!"),
            old_state @ RentalState::Closed { .. } => {
                swap(&mut temp_state, old_state);

                let manager = temp_state.unwrap_closed();
                let stock = manager.stock();

                self.state = RentalState::Open { cars: stock };
                Ok(())
            }
        }
    }

    pub fn rent(&mut self) -> Result<RentedCar, anyhow::Error> {
        match self.state {
            RentalState::Open { cars } => {
                if self.cars_leased == cars.0.len() - 1 {
                    bail!("No more cars left to rent");
                }

                self.cars_leased += 1;

                let rented_car = RentedCar {
                    car: &cars.0[self.cars_leased],
                    rental: self,
                };

                Ok(rented_car)
            }
            RentalState::Closed { .. } => bail!("Rental is closed!"),
            RentalState::Opening => bail!("Shop is still opening!"),
        }
    }

    pub fn hand_in(&self) {
        // self.cars_leased -= 1;
    }
}

impl<'a> RentalState<'a> {
    pub fn unwrap_open(self) -> &'a CarStock {
        match self {
            RentalState::Open { cars } => cars,
            RentalState::Closed { .. } => panic!("Tried to unwrap open on closed state"),
            RentalState::Opening => panic!("Tried to unwrap open on opening state"),
        }
    }

    pub fn unwrap_closed(self) -> AfterHoursManager<'a> {
        match self {
            RentalState::Closed { manager } => manager,
            RentalState::Open { .. } => panic!("Tried to unwrap closed on open state"),
            RentalState::Opening => panic!("Tried to unwrap closed on opening state"),
        }
    }
}

impl<'a> AfterHoursManager<'a> {
    pub fn stock(&self) -> &'a CarStock {
        self.stock
    }
}

impl Default for CarKind {
    fn default() -> Self {
        CarKind::Sudan
    }
}

impl Drop for RentedCar<'_, '_, '_> {
    fn drop(&mut self) {
        self.rental.hand_in();
    }
}

fn main() {
    let cars = CarStock(vec![
        Car::default(),
        Car::default(),
        Car::default(),
        Car::default(),
    ]);

    let rental = Rental::new(&cars);
}
